Most streams found with [radio-browser.info](https://www.radio-browser.info)

+ **VLC** > Media > Open Network Stream > [paste]

    + [music] https://codeberg.org/mononym/radio-stations/raw/branch/main/music.m3u 

    + [words] https://codeberg.org/mononym/radio-stations/raw/branch/main/words.m3u

+ **pyradio** > .bashrc > [paste]

    + [music] alias radio='wget -P ~/.cache/pyradio https://codeberg.org/mononym/radio-stations/raw/branch/main/music.csv && ~/Software/Miniconda3/envs/pyradio/bin/pyradio -s ~/.cache/pyradio/music.csv && rm ~/.cache/pyradio/music.csv'

    + open terminal and run `$ radio`